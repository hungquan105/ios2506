//
//  BaseNaviSearchViewController.swift
//  tabbar++
//
//  Created by Quân on 8/24/19.
//  Copyright © 2019 QuanNguyen. All rights reserved.
//

import UIKit

class BaseNaviSearchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupNavi()
    }
    
    func setupNavi() {
        let search:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchAction))
        self.navigationItem.rightBarButtonItem = search
    }
    
    @objc func searchAction() {
        print("search")
    }

}
