//
//  BaseTabbarViewController.swift
//  tabbar++
//
//  Created by Quân on 8/24/19.
//  Copyright © 2019 QuanNguyen. All rights reserved.
//

import UIKit

class BaseTabbarViewController: UITabBarController {

    // xac dinh storyboard
    let sb = UIStoryboard(name: "Main", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTabbar()
    }
    
    func setupTabbar() {
        
        // xac dinh viewcontroller
        let home = sb.instantiateViewController(withIdentifier: "HomeViewController")
        // tao item
        home.tabBarItem = UITabBarItem(title: "Trang Chủ", image: #imageLiteral(resourceName: "home"), tag: 100)
        // gan navigation
        let navHome = BaseNavigationViewController(rootViewController: home)
        
        let favorites = sb.instantiateViewController(withIdentifier: "MyStoreViewController")
        favorites.tabBarItem = UITabBarItem(title: "Gian Hàng", image: #imageLiteral(resourceName: "gift"), tag: 200)
        let navFavorites = BaseNavigationViewController(rootViewController: favorites)
        
        let more = sb.instantiateViewController(withIdentifier: "MyCartViewController")
        more.tabBarItem = UITabBarItem(title: "Giỏ Hàng", image: #imageLiteral(resourceName: "shopping-cart"), tag: 300)
        let navMore = BaseNavigationViewController(rootViewController: more)
        
        let profile = sb.instantiateViewController(withIdentifier: "ProfileViewController")
        profile.tabBarItem = UITabBarItem(title: "Cá Nhân", image: #imageLiteral(resourceName: "user"), tag: 400)
        let navProfile = BaseNavigationViewController(rootViewController: profile)
        
        // them UIViewController vao mang viewControllers
        self.viewControllers = [navHome, navFavorites, navMore, navProfile]
    }

}
