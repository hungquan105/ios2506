//
//  BannerCell.swift
//  tabbar++
//
//  Created by Quân on 9/10/19.
//  Copyright © 2019 QuanNguyen. All rights reserved.
//

import UIKit
import FSPagerView

class BannerCell: UITableViewCell {
    
    var arrImage: [String] = [
        "https://www.founditgd.com/image/catalog/Ecommerce-Web-Design-Banner-fix.png",
        "https://websightpro.com/wp-content/uploads/2019/03/ecommerce-banner.png",
        "https://www.patroninfosystem.com/header_banner/sub_menu_banner11.png",
        "https://sudospaces.com/chanhtuoi-com/uploads/2015/07/khong-gi-re-hon-the-lazada-99k.jpg",
        "https://ithietkeweb.com/wp-content/uploads/2016/11/2016-06-25-1466835058-3172856-DKCWebDesignBanner.jpg",
        "https://www.founditgd.com/image/catalog/Ecommerce-Web-Design-Banner-fix.png"
    ]
    
    @IBOutlet weak var pageControl: FSPageControl! {
        didSet {
            self.pageControl.numberOfPages = arrImage.count
            self.pageControl.setFillColor(#colorLiteral(red: 0.01822317205, green: 0.8342264295, blue: 0.9688803554, alpha: 1), for: .selected)
            self.pageControl.setFillColor(.white, for: .normal)
        }
    }
    @IBOutlet weak var slider: FSPagerView! {
        didSet {
            self.slider.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.slider.automaticSlidingInterval = 3.0
            self.slider.isInfinite = true
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension BannerCell: FSPagerViewDelegate, FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return arrImage.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let ban = arrImage[index]
        cell.imageView?.kfImageURL(ban ?? "", placeHolder: nil)
        cell.imageView?.contentMode = .scaleAspectFit
        //        cell.imageView?.kfImageURL(arrImage[index], placeHolder: nil)
        return cell
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }
}
