//
//  CategoriesItemCell.swift
//  tabbar++
//
//  Created by Quân on 9/10/19.
//  Copyright © 2019 QuanNguyen. All rights reserved.
//

import UIKit

class CategoriesItemCell: UICollectionViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
}
