//
//  ProductsItemCell.swift
//  tabbar++
//
//  Created by Quân on 9/10/19.
//  Copyright © 2019 QuanNguyen. All rights reserved.
//

import UIKit

class ProductsItemCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lbProduct: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
}
