//
//  LoginViewController.swift
//  tabbar++
//
//  Created by Quân on 9/5/19.
//  Copyright © 2019 QuanNguyen. All rights reserved.
//

import UIKit

class LoginViewController: BaseNaviViewController {

    //MARK: IBOUTLETS
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    
    //MARK: OTHER VARIABLES
    
    
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        setupVar()
        callAPI()
    }
    
    //MARK: - SETUP VAR
    func setupVar() {
        
    }
    
    //MARK: - SETUP UI
    func setupUI() {
        
    }
    
    //MARK: - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL AND BIND DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS
    @IBAction func LOGIN(_ sender: Any) {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.login()
    }
    
}
